local WorldAgeStorage = Class(function(self, inst)
    self.inst = inst
    self.worldage = TheWorld.components.worldstate:GetWorldAge()
end)

function WorldAgeStorage:OnSave()
	return
    {
		worldage = self.worldage
	}
end

function WorldAgeStorage:OnLoad(data)
	if data ~= nil then
		self.worldage = data.worldage or self.worldage
	end
end

function WorldAgeStorage:GetAge()
    local worldage = TheWorld.components.worldstate:GetWorldAge()
    return worldage - self.worldage
end

function WorldAgeStorage:GetDebugString()
    return string.format("age: %.2f", self:GetAge())
end

return WorldAgeStorage
