local cleaning_prefabs = TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS

local function IsInsideInventoryOrContainer(inst)
    if inst.components.inventoryitem and inst.components.inventoryitem.owner ~= nil then 
        return true
    end
    return false
end

local function CanRemove(inst, data, worldcount)
    local result = true
    local x, y, z = inst.Transform:GetWorldPosition()

    if type(data) == "table" then
        if data.worldcount_limit and data.worldcount_limit >= worldcount then
            result = false
        end

        if result and not data.myhome_noprotection and TheWorld.net.components.myhome_manager then
            local home = TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
            if home then
                result = false
            end
        end

        if result and data.percent_limit then
            local percent = nil

            if inst.components.finiteuses then
                percent = inst.components.finiteuses:GetPercent() * 100
            elseif inst.components.perishable then
                percent = inst.components.perishable:GetPercent() * 100
            elseif inst.components.fueled then
                percent = inst.components.fueled:GetPercent() * 100
            elseif inst.components.armor then
                percent = inst.components.armor:GetPercent() * 100
            end

            if percent and percent >= data.percent_limit then
                result = false
            end
        end

        if result and data.age_limit and
           inst.components.worldagestorage and
           inst.components.worldagestorage:GetAge() <= data.age_limit then
            result = false
        end
    elseif TheWorld.net.components.myhome_manager then
        local home = TheWorld.net.components.myhome_manager:GetHomeByPosition(x, y, z)
        if home then
            result = false
        end
    end

    return result
end

function CompareAge(a, b)
    if a.components.worldagestorage and b.components.worldagestorage then
        return a.components.worldagestorage:GetAge() < b.components.worldagestorage:GetAge()
    end
    return false
end

local function DoClean(self)
    local data = nil
    local worldcount = nil
    local prefab_insts = {}
    local prefab_worldcount = {}

    for _, ent in pairs(Ents) do
        if cleaning_prefabs[ent.prefab] ~= nil then
            local tbl_insts = prefab_insts[ent.prefab]
            if tbl_insts == nil then
                prefab_insts[ent.prefab] = {}
                tbl_insts = prefab_insts[ent.prefab]
            end
            table.insert(tbl_insts, ent)
            worldcount = prefab_worldcount[ent.prefab]
            if worldcount == nil then
                worldcount = 1
            else
                worldcount = worldcount + 1
            end
            prefab_worldcount[ent.prefab] = worldcount
        end
    end

    for prefab, list in pairs(prefab_insts) do
        data = cleaning_prefabs[prefab]
        worldcount = prefab_worldcount[prefab]
        table.sort(list, CompareAge)
        for _, ent in ipairs(list) do
            if data and not ent.inlimbo and
               not IsInsideInventoryOrContainer(ent)
               and ent.Transform and CanRemove(ent, data, worldcount) then
                ent:Remove()
                worldcount = worldcount - 1
            end
        end
        prefab_worldcount[prefab] = worldcount
    end

    local shardid = tostring(TheShard:GetShardId() or 0) --Note: GetShardId function return string type
    if shardid ~= "0" then
        TheNet:Announce(string.format(STRINGS.WORLDTRASHCLEANER.AFTER_CLEANING_IN_SHARD_WORLD, shardid), nil, nil, "default")
    else
        TheNet:Announce(STRINGS.WORLDTRASHCLEANER.AFTER_CLEANING, nil, nil, "default")
    end

    self.lastcycle = TheWorld.state.cycles
    self.running = false
end

local function CleanDelay(self)
    if self.running then return end

    self.running = true

    local shardid = tostring(TheShard:GetShardId() or 0) --Note: GetShardId function return string type
    if shardid ~= "0" then
        TheNet:Announce(string.format(STRINGS.WORLDTRASHCLEANER.BEFORE_CLEANING_IN_SHARD_WORLD, shardid), nil, nil, "default")
    else
        TheNet:Announce(STRINGS.WORLDTRASHCLEANER.BEFORE_CLEANING, nil, nil, "default")
    end

    self.inst:DoTaskInTime(30, function(inst) DoClean(self) end)
end

local function OnCyclesChanged(self, cycle)
    if TUNING.WORLDTRASHCLEANER.CLEANING_PERIOD ~= 0 then
        local period = cycle - self.lastcycle

        if period == TUNING.WORLDTRASHCLEANER.CLEANING_PERIOD - 1 then
            local shardid = tostring(TheShard:GetShardId() or 0) --Note: GetShardId function return string type
            if shardid ~= "0" then
                TheNet:Announce(string.format(STRINGS.WORLDTRASHCLEANER.ANNOUNCE_IN_SHARD_WORLD, shardid), nil, nil, "default")
            else
                TheNet:Announce(STRINGS.WORLDTRASHCLEANER.ANNOUNCE, nil, nil, "default")
            end
        end

        if period >= TUNING.WORLDTRASHCLEANER.CLEANING_PERIOD then
            CleanDelay(self)
        end
    end
end

local WorldTrashCleaner = Class(function(self, inst)
    self.inst = inst
    self.lastcycle = TheWorld.state.cycles
    self.running = false

    -- init
    self.inst:DoTaskInTime(0, function(inst)
        inst:ListenForEvent("cycleschanged", function(world, cycle)
            OnCyclesChanged(self, cycle)
        end)
    end)
end)

function WorldTrashCleaner:OnSave()
    return
    {
        lastcycle = self.lastcycle
    }
end

function WorldTrashCleaner:OnLoad(data)
    if data ~= nil then
        self.lastcycle = data.lastcycle or self.lastcycle
    end
end

function WorldTrashCleaner:DoForceClean()
    CleanDelay(self)
end

return WorldTrashCleaner
