--English
GLOBAL.STRINGS.WORLDTRASHCLEANER =
{
    ANNOUNCE = "Attention! Trash will be cleared next day.",
    ANNOUNCE_IN_SHARD_WORLD = "Attention! In world %s, trash will be cleared next day.",
    BEFORE_CLEANING = "Trash will be cleared after 30 s.",
    BEFORE_CLEANING_IN_SHARD_WORLD = "In world %s, trash will be cleared after 30 s.",
    AFTER_CLEANING = "Trash is now cleaning.",
    AFTER_CLEANING_IN_SHARD_WORLD = "In world %s, trash is now cleaning.",
}
