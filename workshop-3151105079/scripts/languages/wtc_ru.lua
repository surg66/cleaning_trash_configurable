--Russian
GLOBAL.STRINGS.WORLDTRASHCLEANER =
{
    ANNOUNCE = "Внимание! Мусор будет удалён на следующий день.",
    ANNOUNCE_IN_SHARD_WORLD = "Внимание! В мире %s, мусор будет удалён на следующий день.",
    BEFORE_CLEANING = "Мусор будет удалён через 30 секунд.",
    BEFORE_CLEANING_IN_SHARD_WORLD = "В мире %s, мусор будет удалён через 30 секунд.",
    AFTER_CLEANING = "Мусор удалён.",
    AFTER_CLEANING_IN_SHARD_WORLD = "В мире %s, мусор удалён.",
}
