name = "Cleaning trash (configurable)"
description = [[Periodically cleans items in the world, compatible with MyHome mod (default forbidden remove items inside MyHome).

For admin available command "/cleantrash".

Extended items to remove configure in modoverrides.lua
For example configure see modinfo.lua

Mod is focused on personal configuration of dedicated servers. By default has a minimum items for remove.]]
author = "surg"
version = "1.0.2"
forumthread = ""
api_version_dst = 10
icon_atlas = "modicon.xml"
icon = "modicon.tex"
priority = 0
dont_starve_compatible = false
reign_of_giants_compatible = false
dst_compatible = true
all_clients_require_mod = true
client_only_mod = false
server_filter_tags = {}

local config_cleaning_prefabs =
{
    -- include or override
    -- opts = {myhome_noprotection = true, percent_limit = 50, age_limit = 10 worldcount_limit = 100}
    INCLUDE =
    {
        --[[
        -- for example
        ["torch"] = {percent_limit = 30},
        ["shadowheart"] = {age_limit = 15, worldcount_limit = 5},
        ["armorwood"] = {myhome_noprotection = true, percent_limit = 50},
        ["spiderhat"] = {worldcount_limit = 5},
        ["flower"] = {worldcount_limit = 400},
        ["carrot_planted"] = {worldcount_limit = 200},
        ["phlegm"] = {worldcount_limit = 20},
        "flint",
        "rocks",
        ]]
    },

    EXCLUDE =
    {
        -- for example
        --[[
        "flint",
        "rocks",
        "shadowheart",
        ]]
    },
}

configuration_options =
{
    {
        name = "language",
        label = "Language",
        hover = "Default language",
        options =
        {
            {description = "English", data = "en"},
            {description = "Русский", data = "ru"},
        },
        default = "en",
    },

    {
        name = "cleaning_period",
        label = "Cleaning period",
        hover = "Cleaning period",
        options =
        {
            {description = "Off",     data = 0},
            {description = "1 day",   data = 1},
            {description = "3 days",  data = 3},
            {description = "7 days",  data = 7},
            {description = "10 days", data = 10},
            {description = "15 days", data = 15},
            {description = "20 days", data = 20},
            {description = "25 days", data = 25},
            {description = "30 days", data = 30},
        },
        default = 10
    },
    {
        name = "config_cleaning_prefabs",
        label = "Configure prefabs for clean",
        hover = "Avaiable for configure in modoverrides.lua",
        options =
        {
            {description = "Classified", data = config_cleaning_prefabs}
        },
        default = config_cleaning_prefabs
    }
}
