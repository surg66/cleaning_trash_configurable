GLOBAL.TUNING.WORLDTRASHCLEANER = {}
GLOBAL.TUNING.WORLDTRASHCLEANER.LANGUAGE = GetModConfigData("language")
GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PERIOD = GetModConfigData("cleaning_period")
GLOBAL.TUNING.WORLDTRASHCLEANER.CONFIG_CLEANING_PREFABS = GetModConfigData("config_cleaning_prefabs")
GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS = {}

modimport("scripts/languages/wtc_"..GLOBAL.TUNING.WORLDTRASHCLEANER.LANGUAGE..".lua")

local list =
{
    ["twigs"]           = {worldcount_limit = 100},
    ["rocks"]           = {worldcount_limit = 100},
    ["flint"]           = {worldcount_limit = 100},
    ["cutgrass"]        = {worldcount_limit = 100},
    ["charcoal"]        = {worldcount_limit = 20},
    ["poop"]            = {worldcount_limit = 20},
    ["guano"]           = {worldcount_limit = 20},
    ["spoiled_food"]    = {worldcount_limit = 20},
    ["twiggy_nut"]      = {worldcount_limit = 20},
    ["pinecone"]        = {worldcount_limit = 20},
    ["acorn"]           = {worldcount_limit = 20},
    ["mosquitosack"]    = {worldcount_limit = 20},
    ["steelwool"]       = {worldcount_limit = 5},
    ["beefalowool"]     = {worldcount_limit = 20},
    ["horn"]            = {worldcount_limit = 10},
    ["silk"]            = {worldcount_limit = 20},
    ["spidergland"]     = {worldcount_limit = 20},
    ["skeleton_player"] = {age_limit = 15, worldcount_limit = 10},
    ["boneshard"]       = {worldcount_limit = 10},
    ["houndstooth"]     = {worldcount_limit = 10},
    ["stinger"]         = {worldcount_limit = 10},    
    ["feather_crow"]    = {worldcount_limit = 5},
    ["feather_canary"]  = {worldcount_limit = 5},
    ["feather_robin"]   = {worldcount_limit = 5},
    ["feather_robin_winter"] = {worldcount_limit = 5},

	"yotp_food1",
	"yotp_food2",
	"yotp_food3",

	"winter_food1",
	"winter_food2",
	"winter_food3",
	"winter_food4",
	"winter_food5",
	"winter_food6",
	"winter_food7",
	"winter_food8",
	"winter_food9",
	"wintersfeastfuel",
	"crumbs",

	"winter_ornament_plain1",
	"winter_ornament_plain2",
	"winter_ornament_plain3",
	"winter_ornament_plain4",
	"winter_ornament_plain5",
	"winter_ornament_plain6",
	"winter_ornament_plain7",
	"winter_ornament_plain8",
	"winter_ornament_plain9",
	"winter_ornament_plain10",
	"winter_ornament_plain11",
	"winter_ornament_plain12",
	"winter_ornament_fancy1",
	"winter_ornament_fancy2",
	"winter_ornament_fancy3",
	"winter_ornament_fancy4",
	"winter_ornament_fancy5",
	"winter_ornament_fancy6",
	"winter_ornament_fancy7",
	"winter_ornament_fancy8",
	"winter_ornament_boss_bearger",
	"winter_ornament_boss_deerclops",
	"winter_ornament_boss_moose",
	"winter_ornament_boss_dragonfly",
	"winter_ornament_boss_beequeen",
	"winter_ornament_boss_antlion",
	"winter_ornament_boss_fuelweaver",
	"winter_ornament_boss_klaus",
	"winter_ornament_boss_malbatross",
	"winter_ornament_boss_krampus",
	"winter_ornament_boss_noeyered",
	"winter_ornament_boss_noeyeblue",
	"winter_ornament_boss_crabking",
	"winter_ornament_boss_crabkingpearl",
	"winter_ornament_boss_hermithouse",
	"winter_ornament_boss_minotaur",
	"winter_ornament_boss_pearl",
	"winter_ornament_boss_wagstaff",
    "winter_ornament_boss_toadstool",
    "winter_ornament_boss_toadstool_misery",
	"winter_ornament_festivalevents1",
	"winter_ornament_festivalevents2",
	"winter_ornament_festivalevents3",
	"winter_ornament_festivalevents4",
	"winter_ornament_festivalevents5",
	"winter_ornament_boss_celestialchampion1",	
	"winter_ornament_boss_celestialchampion2",
	"winter_ornament_boss_celestialchampion3",
    "winter_ornament_boss_celestialchampion4",
	"winter_ornament_boss_eyeofterror1",
    "winter_ornament_boss_eyeofterror2",

	"winter_ornament_light1",
	"winter_ornament_light2",
	"winter_ornament_light3",
	"winter_ornament_light4",
	"winter_ornament_light5",
	"winter_ornament_light6",
	"winter_ornament_light7",
	"winter_ornament_light8",

	"halloweencandy_1",
	"halloweencandy_2",
	"halloweencandy_3",
	"halloweencandy_4",
	"halloweencandy_5",
	"halloweencandy_6",
	"halloweencandy_7",
	"halloweencandy_8",
	"halloweencandy_9",
	"halloweencandy_10",
	"halloweencandy_11",
	"halloweencandy_12",
	"halloweencandy_13",
	"halloweencandy_14",

	"halloween_ornament_1",
	"halloween_ornament_2",
	"halloween_ornament_3",
	"halloween_ornament_4",
	"halloween_ornament_5",
	"halloween_ornament_6",

	"trinket_1",
	"trinket_2",
	"trinket_3",
	"trinket_4",
	"trinket_5",
	"trinket_6",
	"trinket_7",
	"trinket_8",
	"trinket_9",
	"trinket_10",
	"trinket_11",
	"trinket_12",
	"trinket_13",
	"trinket_14",
	"trinket_15",
	"trinket_16",
	"trinket_17",
	"trinket_18",
	"trinket_19",
	"trinket_20",
	"trinket_21",
	"trinket_22",
	"trinket_23",
	"trinket_24",
	"trinket_25",
	"trinket_26",
	"trinket_27",
	"trinket_28",
	"trinket_29",
	"trinket_30",
	"trinket_31",
    "trinket_32",
	"trinket_33",
	"trinket_34",
	"trinket_35",
	"trinket_36",
	"trinket_37",
	"trinket_38",
	"trinket_39",
	"trinket_40",
	"trinket_41",
	"trinket_42",
	"trinket_43",
	"trinket_44",
	"trinket_45",
	"trinket_46",
	"cotl_trinket",
    "antliontrinket",
}

for k, v in pairs(list) do
    if type(k) == "number" and type(v) == "string" then
        GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS[v] = true
    elseif type(k) == "string" and type(v) == "table" then
        GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS[k] = v
    end
end

for k, v in pairs(TUNING.WORLDTRASHCLEANER.CONFIG_CLEANING_PREFABS.INCLUDE) do
    if type(k) == "number" and type(v) == "string" then
        GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS[v] = true
    elseif type(k) == "string" and type(v) == "table" then
        GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS[k] = v
    end
end

for _, v in ipairs(TUNING.WORLDTRASHCLEANER.CONFIG_CLEANING_PREFABS.EXCLUDE) do
    GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS[v] = nil
end

for k, v in pairs(GLOBAL.TUNING.WORLDTRASHCLEANER.CLEANING_PREFABS) do
    if type(v) == "table" and v.age_limit ~= nil then    
        AddPrefabPostInit(k, function(inst)
            if not GLOBAL.TheWorld.ismastersim then
                return inst
            end
            inst:AddComponent("worldagestorage")
        end)
    end
end

AddPrefabPostInit("world", function(inst)
    if not inst.ismastersim then
        return
    end
    inst:AddComponent("worldtrashcleaner")
end)

-- global
GLOBAL.DoForceCleanTrashInWorld = function()
    if GLOBAL.TheWorld and GLOBAL.TheWorld.ismastersim and GLOBAL.TheWorld.components.worldtrashcleaner then
        GLOBAL.TheWorld.components.worldtrashcleaner:DoForceClean()
    end
end

AddUserCommand("cleantrash", {
    prettyname = "Clean trash", 
    desc = "Clean trash", 
    permission = GLOBAL.COMMAND_PERMISSION.ADMIN,
    slash = true,
    usermenu = false,
    servermenu = false,
    params = {},
    vote = false,
    serverfn = function(params, caller)
        GLOBAL.DoForceCleanTrashInWorld()
    end,
})
