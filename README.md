# Desctiption
Cleaning trash (configurable) - Don't Starve Together modification.

Periodically cleans items in the world, compatible with MyHome mod (default forbidden remove items inside MyHome).

Default items to remove: twigs, rocks, flint, cutgrass, charcoal, poop, guano, spoiled_food, twiggy_nut, pinecone, acorn, mosquitosack, steelwool, beefalowool, horn, silk, spidergland, skeleton_player, boneshard, houndstooth, stinger, feathers, yotp_foods, winter_foods, wintersfeastfuel, crumbs, winter ornaments, halloween ornaments, trinkets.

Extended items to remove configure in modoverrides.lua

For admin available command "/cleantrash".

Configure have two section INCLUDE and EXCLUDE.
1) INCLUDE - list items with options for remove.
2) EXCLUDE - list items which cancel remove.

Options for item:
1) myhome_noprotection (boolean) - allows remove item inside MyHome.
2) percent_limit (number [1..100]) - allows remove item if percent uses less.
3) age_limit (number) - allows remove item if age more.
4) worldcount_limit (number) - how many items will remain in the world after remove.

For example (see modinfo.lua):
INCLUDE = {
  ["torch"] = {percent_limit = 30},
  ["shadowheart"] = {age_limit = 15, worldcount_limit = 5},
  ["armorwood"] = {myhome_noprotection = true, percent_limit = 50},
  "flower",
}

Mod is focused on personal configuration of dedicated servers. By default has a minimum items for remove.


[Link to mod in Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=3151105079)